<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index()
    {

        // use this instagram access token generator http://instagram.pixelunion.net/
        $access_token="13296682303.1677ed0.7bd9cf2ecbbd46819f40b9055f200420";
        $photo_count=11;

        $json_link="https://api.instagram.com/v1/users/self/media/recent/?";
        $json_link.="access_token={$access_token}&count={$photo_count}";
        $json = file_get_contents($json_link);
        $obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);

//        dump($obj['data']);
//        die();
        return $this->render('default/index.html.twig', [
            'pictures' => $obj['data'],
        ]);
    }
    /**
     * @Route("/partners")
     */

    public function partners()
    {
        return $this->render('registration/partners.html.twig');
    }
}
